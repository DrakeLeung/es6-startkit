#es6-startkit
No react. Just for writing soem demos using ES6 :smile:

## Run
1. install dependencies: `npm install`
2. run webpack dev server: `npm start`(check `script` property out in the `package.json`)
3. open browser: `http://localhost:3000/`

## Feature
- Write ES6 with the help of babel
- Live editting becuz webpack dev server
- CSS loader
